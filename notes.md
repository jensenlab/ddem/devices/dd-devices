# Notes

## Publisher.js

_version device_

### Input format to `.record()`

```javascript
const data = {
    "tmpl_XXX": {
        field1: value1,
        field2: value2
    },
    "tmpl_YYY": {
        field1: value3,
        field2: value4
    }
}
```
